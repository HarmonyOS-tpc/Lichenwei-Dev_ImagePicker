/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.demo.imagepicker;

import com.bumptech.glide.Glide;
import com.lcw.library.imagepicker.utils.ImageLoader;
import ohos.agp.components.Image;
import ohos.utils.net.Uri;

public class GlideLoader implements ImageLoader {

    @Override
    public void loadImage(Image imageView, String imagePath) {
        Glide.with(imageView.getContext()).load(imagePath).into(imageView);
    }

    @Override
    public void loadPreImage(Image imageView, String imagePath) {
        Glide.with(imageView.getContext()).load(imagePath).into(imageView);
    }

    @Override
    public void loadImageUri(Image imageView, Uri uri) {
        Glide.with(imageView.getContext()).load(uri).into(imageView);
    }

    @Override
    public void loadImageResource(Image imageView, int imageId) {
        Glide.with(imageView.getContext()).load(imageId).into(imageView);
    }

    @Override
    public void clearMemoryCache() {
        Glide.get(new MyApplication().getContext()).clearMemory();
    }
}
