package com.lcw.library.imagepicker.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import org.jetbrains.annotations.Nullable;

/**
 * 正方形RelativeLayout
 * Create by: chenWei.li
 * Date: 2018/9/1
 * Time: 下午10:12
 * Email: lichenwei.me@foxmail.com
 */
public class SquareRelativeLayout extends DependentLayout implements Component.DrawTask {
    private static final String TAG = SquareRelativeLayout.class.getSimpleName();

    public SquareRelativeLayout(Context context) {
        this(context, null);
        addDrawTask(this);
    }

    public SquareRelativeLayout(Context context, @Nullable AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SquareRelativeLayout(Context context, @Nullable AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
    }
}
