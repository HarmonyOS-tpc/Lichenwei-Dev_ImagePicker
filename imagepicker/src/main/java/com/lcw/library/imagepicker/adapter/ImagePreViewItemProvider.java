/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.library.imagepicker.adapter;

import com.lcw.library.imagepicker.ResourceTable;
import com.lcw.library.imagepicker.data.MediaFile;
import com.lcw.library.imagepicker.manager.ConfigManager;
import com.lcw.library.imagepicker.utils.ResUtil;
import com.lcw.library.imagepicker.view.PinchImageView;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.LinkedList;
import java.util.List;

public class ImagePreViewItemProvider extends PageSliderProvider {
    private Context mContext;
    private List<MediaFile> mMediaFileList;
    private LinkedList<PinchImageView> viewCache = new LinkedList<>();

    public ImagePreViewItemProvider(Context context, List<MediaFile> mediaFileList) {
        this.mContext = context;
        this.mMediaFileList = mediaFileList;
    }

    @Override
    public int getCount() {
        return mMediaFileList == null ? 0 : mMediaFileList.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        PinchImageView imageView;
        if (viewCache.size() > 0) {
            imageView = viewCache.remove();
            imageView.release();
        } else {
            imageView = new PinchImageView(mContext);
        }
        MediaFile mediaFile = mMediaFileList.get(position);
        try {
            imageView.setScaleMode(Image.ScaleMode.STRETCH);
            if (ResUtil.getElementByResId(mContext, ResourceTable.Media_icon_image_default).isPresent()) {
                imageView.setBackground(ResUtil.getElementByResId(mContext, ResourceTable.Media_icon_image_default).get());
            }
            ConfigManager.getInstance().getImageLoader().loadImageUri(imageView, mediaFile.getExternalUri());
        } catch (Exception e) {
            e.printStackTrace();
        }
        componentContainer.addComponent(imageView);
        return imageView;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        PinchImageView imageView = (PinchImageView) o;
        componentContainer.removeComponent(imageView);
        viewCache.add(imageView);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }
}
