package com.lcw.library.imagepicker.data;

import ohos.utils.net.Uri;

/**
 * ý��ʵ����
 * Create by: chenWei.li
 * Date: 2018/8/22
 * Time: ����12:36
 * Email: lichenwei.me@foxmail.com
 */
public class MediaFile {

    private String path;
    private String mime;
    private Integer folderId;
    private String folderName;
    private long duration;
    private long dateTaken;
    private Uri externalUri;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(long dateTaken) {
        this.dateTaken = dateTaken;
    }

    public Uri getExternalUri() {
        return externalUri;
    }

    public void setExternalUri(Uri externalUri) {
        this.externalUri = externalUri;
    }
}

