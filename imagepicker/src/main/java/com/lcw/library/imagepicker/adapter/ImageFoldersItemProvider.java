/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.library.imagepicker.adapter;

import com.lcw.library.imagepicker.ResourceTable;
import com.lcw.library.imagepicker.data.MediaFolder;
import com.lcw.library.imagepicker.manager.ConfigManager;
import com.lcw.library.imagepicker.utils.ResUtil;
import com.lcw.library.imagepicker.utils.TextUtils;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.Image;
import ohos.app.Context;

import java.util.List;

public class ImageFoldersItemProvider extends BaseItemProvider {
    private Context mContext;
    private List<MediaFolder> mMediaFolderList;
    private int mCurrentImageFolderIndex;
    private LayoutScatter mLayoutScatter;

    public ImageFoldersItemProvider(Context context, List<MediaFolder> mediaFolderList, int position) {
        this.mContext = context;
        this.mMediaFolderList = mediaFolderList;
        this.mCurrentImageFolderIndex = position;
        mLayoutScatter = LayoutScatter.getInstance(context);
    }

    @Override
    public int getCount() {
        return mMediaFolderList == null ? 0 : mMediaFolderList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = mLayoutScatter.parse(ResourceTable.Layout_item_listcontainer_folder, null, false);
        }
        FolderViewHolder viewHolder = new FolderViewHolder(component);
        final MediaFolder mediaFolder = mMediaFolderList.get(position);
        String folderCover = mediaFolder.getFolderCover();
        String folderName = mediaFolder.getFolderName();
        int imageSize = mediaFolder.getMediaFileList().size();

        if (!TextUtils.isEmpty(folderName)) {
            viewHolder.mFolderName.setText(folderName);
        }

        viewHolder.mImageSize.setText(String.format(ResUtil.getString(mContext, ResourceTable.String_image_num), imageSize));

        if (mCurrentImageFolderIndex == position) {
            viewHolder.mImageFolderCheck.setVisibility(Component.VISIBLE);
        } else {
            viewHolder.mImageFolderCheck.setVisibility(Component.HIDE);
        }
        try {
            ConfigManager.getInstance().getImageLoader().loadImage(viewHolder.mImageCover, folderCover);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mImageFolderChangeListener != null) {
            viewHolder.itemView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    mCurrentImageFolderIndex = position;
                    notifyDataChanged();
                    mImageFolderChangeListener.onImageFolderChange(component, position);
                }
            });
        }
        return component;
    }

    static class FolderViewHolder {
        Component itemView;
        private Image mImageCover, mImageFolderCheck;
        private Text mFolderName, mImageSize;

        FolderViewHolder(Component component) {
            this.itemView = component;
            mImageCover = (Image) itemView.findComponentById(ResourceTable.Id_iv_item_imageCover);
            mFolderName = (Text) itemView.findComponentById(ResourceTable.Id_tv_item_folderName);
            mImageSize = (Text) itemView.findComponentById(ResourceTable.Id_tv_item_imageSize);
            mImageFolderCheck = (Image) itemView.findComponentById(ResourceTable.Id_iv_item_check);
        }
    }

    private OnImageFolderChangeListener mImageFolderChangeListener;

    public void setOnImageFolderChangeListener(OnImageFolderChangeListener onItemClickListener) {
        this.mImageFolderChangeListener = onItemClickListener;
    }

    public interface OnImageFolderChangeListener {
        void onImageFolderChange(Component view, int position);
    }
}
