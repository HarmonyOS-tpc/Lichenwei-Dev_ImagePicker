package com.lcw.library.imagepicker.loader;

import com.lcw.library.imagepicker.data.MediaFile;
import ohos.aafwk.ability.DataUriUtils;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

/**
 * 媒体库扫描类(视频)
 * Create by: chenWei.li
 * Date: 2018/8/21
 * Time: 上午1:01
 * Email: lichenwei.me@foxmail.com
 */
public class VideoScanner extends AbsMediaScanner<MediaFile> {

    public VideoScanner(Context context) {
        super(context);
    }

    @Override
    protected Uri getScanUri() {
        return AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI;
    }

    @Override
    protected String[] getProjection() {
        return new String[]{
                AVStorage.Video.Media.DATA,
                AVStorage.Video.Media.MIME_TYPE,
                AVStorage.Video.Media.ID,
                AVStorage.Video.Media.DISPLAY_NAME,
                AVStorage.Video.Media.DURATION,
                AVStorage.Video.Media.DATE_ADDED
        };
    }

    @Override
    protected String getSelection() {
        return null;
    }

    @Override
    protected String[] getSelectionArgs() {
        return null;
    }

    @Override
    protected String getOrder() {
        return AVStorage.Video.Media.DATE_ADDED + " desc";
    }

    @Override
    protected MediaFile parse(ResultSet cursor) {
        String path = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.DATA));
        String mime = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.MIME_TYPE));
        int folderId = cursor.getInt(cursor.getColumnIndexForName(AVStorage.Video.Media.ID));
        String folderName = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.DISPLAY_NAME));
        long duration = cursor.getLong(cursor.getColumnIndexForName(AVStorage.Video.Media.DURATION));
        long dateToken = cursor.getLong(cursor.getColumnIndexForName(AVStorage.Video.Media.DATE_ADDED));
        Uri externalUri = DataUriUtils.attachId(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI, folderId);

        MediaFile mediaFile = new MediaFile();
        mediaFile.setPath(path);
        mediaFile.setMime(mime);
        mediaFile.setFolderId(folderId);
        mediaFile.setFolderName(folderName);
        mediaFile.setDuration(duration);
        mediaFile.setDateTaken(dateToken);
        mediaFile.setExternalUri(externalUri);

        return mediaFile;
    }
}
