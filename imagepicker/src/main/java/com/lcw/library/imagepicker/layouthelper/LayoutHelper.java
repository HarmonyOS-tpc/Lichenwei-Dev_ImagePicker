/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.library.imagepicker.layouthelper;

import ohos.agp.components.element.ShapeElement;

public abstract class LayoutHelper {
    /**
     * Return children count
     *
     * @return the number of children
     */
    public abstract int getItemCount();


    public abstract void setBackground(ShapeElement background);

    public abstract ShapeElement getBackground();

    public abstract void setBackground1(ShapeElement background);

    public abstract ShapeElement getBackground1();

    public abstract void setHeight(int height);

    public abstract int getHeight();

    /**
     *
     * Set items' count
     *
     * @param itemCount how many children in this layoutHelper
     */
    public abstract void setItemCount(int itemCount);

    /**
     * Get margins between layout when layout child at <code>offset</code>
     * Or compute offset for align line during scrolling
     *
     * @param offset      anchor child's offset in current layoutHelper, for example, 0 means first item
     * @param isLayoutEnd is the layout process will do to end or start, true means it will lay views from start to end
     * @param useAnchor   whether offset is computed for scrolling or for anchor reset
     * @param helper      view layout helper
     * @return extra offset must be calculated
     */
    public abstract int computeAlignOffset(int offset, boolean isLayoutEnd, boolean useAnchor,
                                           LayoutManagerHelper helper);

    public abstract int computeMarginStart(int offset, boolean isLayoutEnd, boolean useAnchor,
                                           LayoutManagerHelper helper);

    public abstract int computeMarginEnd(int offset, boolean isLayoutEnd, boolean useAnchor,
                                         LayoutManagerHelper helper);

    public abstract int computePaddingStart(int offset, boolean isLayoutEnd, boolean useAnchor,
                                            LayoutManagerHelper helper);

    public abstract int computePaddingEnd(int offset, boolean isLayoutEnd, boolean useAnchor,
                                          LayoutManagerHelper helper);
}
