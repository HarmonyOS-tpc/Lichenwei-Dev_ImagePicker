package com.lcw.library.imagepicker.utils;

import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * 屏幕相关工具类
 * Create by: chenWei.li
 * Date: 2018/8/25
 * Time: 上午1:53
 * Email: lichenwei.me@foxmail.com
 */
public class Utils {
    public static int[] getScreenSize(Context context) {
        Optional<Display> optionalDisplay = DisplayManager.getInstance().getDefaultDisplay(context);
        Display display = optionalDisplay.get();
        Point size = new Point();
        display.getSize(size);
        final int windowWidth = (int) size.position[0];
        final int windowHeight = (int) size.position[1];
        return new int[]{windowWidth, windowHeight};
    }

    public static String getVideoDuration(long timestamp) {
        if (timestamp < 1000) {
            return "00:01";
        }
        Date date = new Date(timestamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
        return simpleDateFormat.format(date);
    }

    public static String getImageTime(long timestamp) {
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(new Date());
        Calendar imageCalendar = Calendar.getInstance();
        imageCalendar.setTimeInMillis(timestamp);
        if (currentCalendar.get(Calendar.DAY_OF_YEAR) == imageCalendar.get(Calendar.DAY_OF_YEAR) && currentCalendar.get(Calendar.YEAR) == imageCalendar.get(Calendar.YEAR)) {
            return "Today";
        } else if (currentCalendar.get(Calendar.WEEK_OF_YEAR) == imageCalendar.get(Calendar.WEEK_OF_YEAR) && currentCalendar.get(Calendar.YEAR) == imageCalendar.get(Calendar.YEAR)) {
            return "This week";
        } else if (currentCalendar.get(Calendar.MONTH) == imageCalendar.get(Calendar.MONTH) && currentCalendar.get(Calendar.YEAR) == imageCalendar.get(Calendar.YEAR)) {
            return "This month";
        } else {
            Date date = new Date(timestamp);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
            return sdf.format(date);
        }
    }

    public static void showToast(Context context, String message) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setAlignment(LayoutAlignment.BOTTOM);
        toastDialog.setText(message);
        toastDialog.show();
    }
}
