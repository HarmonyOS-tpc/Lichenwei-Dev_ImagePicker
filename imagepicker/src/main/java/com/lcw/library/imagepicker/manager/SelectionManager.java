package com.lcw.library.imagepicker.manager;

import com.lcw.library.imagepicker.utils.MediaFileUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 媒体选择集合管理类
 * Create by: chenWei.li
 * Date: 2018/8/23
 * Time: 上午1:19
 * Email: lichenwei.me@foxmail.com
 */
public class SelectionManager {
    private static volatile SelectionManager mSelectionManager;
    private ArrayList<String> mSelectImagePaths = new ArrayList<>();
    private int mMaxCount = 1;

    private SelectionManager() {
    }

    public static SelectionManager getInstance() {
        if (mSelectionManager == null) {
            synchronized (SelectionManager.class) {
                if (mSelectionManager == null) {
                    mSelectionManager = new SelectionManager();
                }
            }
        }
        return mSelectionManager;
    }

    /**
     * Sets the maximum number of selections.
     *
     * @param maxCount
     */
    public void setMaxCount(int maxCount) {
        this.mMaxCount = maxCount;
    }

    /**
     * Obtains the maximum number of selections currently set.
     *
     * @return int
     */
    public int getMaxCount() {
        return this.mMaxCount;
    }

    /**
     * Obtains the path of the selected image set.
     *
     * @return array list
     */
    public ArrayList<String> getSelectPaths() {
        return mSelectImagePaths;
    }

    /**
     * Add/Remove Pictures to Select Collection
     *
     * @param imagePath
     * @return boolean
     */
    public boolean addImageToSelectList(String imagePath) {
        if (mSelectImagePaths.contains(imagePath)) {
            return mSelectImagePaths.remove(imagePath);
        } else {
            if (mSelectImagePaths.size() < mMaxCount) {
                return mSelectImagePaths.add(imagePath);
            } else {
                return false;
            }
        }
    }

    /**
     * Add Picture to Select Collection
     *
     * @param imagePaths
     */
    public void addImagePathsToSelectList(List<String> imagePaths) {
        if (imagePaths != null) {
            for (String imagePath : imagePaths) {
                if (!mSelectImagePaths.contains(imagePath) && mSelectImagePaths.size() < mMaxCount) {
                    mSelectImagePaths.add(imagePath);
                }
            }
        }
    }


    /**
     * Check whether the current picture is selected.
     *
     * @param imagePath
     * @return boolean
     */
    public boolean isImageSelect(String imagePath) {
        if (mSelectImagePaths.contains(imagePath)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Whether to continue to select pictures
     *
     * @return boolean
     */
    public boolean isCanChoose() {
        return getSelectPaths().size() < mMaxCount;
    }

    /**
     * Whether to add to the selection set. (In singleType mode, pictures and videos cannot be selected at the same time.)
     *
     * @param currentPath
     * @param filePath
     * @return boolean
     */
    public static boolean isCanAddSelectionPaths(String currentPath, String filePath) {
        if ((MediaFileUtil.isVideoFileType(currentPath) && !MediaFileUtil.isVideoFileType(filePath)) || (!MediaFileUtil.isVideoFileType(currentPath) && MediaFileUtil.isVideoFileType(filePath))) {
            return false;
        }
        return true;
    }

    /**
     * Clear Selected Images
     */
    public void removeAll() {
        mSelectImagePaths.clear();
    }
}
