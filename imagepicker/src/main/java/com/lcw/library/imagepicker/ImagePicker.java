package com.lcw.library.imagepicker;

import com.lcw.library.imagepicker.manager.ConfigManager;
import com.lcw.library.imagepicker.utils.ImageLoader;
import com.lcw.library.imagepicker.utils.TConstant;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import java.util.ArrayList;

/**
 * 统一调用入口
 * Create by: chenWei.li
 * Date: 2018/8/26
 * Time: 下午6:31
 * Email: lichenwei.me@foxmail.com
 */
public class ImagePicker {
    public static final String EXTRA_SELECT_IMAGES = "selectItems";

    private static volatile ImagePicker mImagePicker;

    private ImagePicker() {
    }

    /**
     * This method is used to get single instance of class
     *
     * @return mImagePicker
     */
    public static ImagePicker getInstance() {
        if (mImagePicker == null) {
            synchronized (ImagePicker.class) {
                if (mImagePicker == null) {
                    mImagePicker = new ImagePicker();
                }
            }
        }
        return mImagePicker;
    }

    /**
     * Set Title
     *
     * @param title
     * @return image picker
     */
    public ImagePicker setTitle(String title) {
        ConfigManager.getInstance().setTitle(title);
        return mImagePicker;
    }

    /**
     * Camera Support
     *
     * @param showCamera
     * @return image picker
     */
    public ImagePicker showCamera(boolean showCamera) {
        ConfigManager.getInstance().setShowCamera(showCamera);
        return mImagePicker;
    }

    /**
     * Display Image
     *
     * @param showImage
     * @return image picker
     */
    public ImagePicker showImage(boolean showImage) {
        ConfigManager.getInstance().setShowImage(showImage);
        return mImagePicker;
    }

    /**
     * Indicates whether to display video.
     *
     * @param showVideo
     * @return image picker
     */
    public ImagePicker showVideo(boolean showVideo) {
        ConfigManager.getInstance().setShowVideo(showVideo);
        return mImagePicker;
    }

    /**
     * Indicates whether to filter GIF images. By default, GIF images are not filtered.
     *
     * @param filterGif
     * @return image picker
     */
    public ImagePicker filterGif(boolean filterGif) {
        ConfigManager.getInstance().setFilterGif(filterGif);
        return mImagePicker;
    }


    /**
     * Maximum number of selected images
     *
     * @param maxCount
     * @return imagepicker
     */
    public ImagePicker setMaxCount(int maxCount) {
        ConfigManager.getInstance().setMaxCount(maxCount);
        return mImagePicker;
    }

    /**
     * Selecting a single type (only images or videos can be selected)
     *
     * @param isSingleType
     * @return imagepicker
     */
    public ImagePicker setSingleType(boolean isSingleType) {
        ConfigManager.getInstance().setSingleType(isSingleType);
        return mImagePicker;
    }


    /**
     * Setting the Image Loader
     *
     * @param imageLoader
     * @return imagepicker
     */
    public ImagePicker setImageLoader(ImageLoader imageLoader) {
        ConfigManager.getInstance().setImageLoader(imageLoader);
        return mImagePicker;
    }

    /**
     * Set Picture Selection History
     *
     * @param imagePaths
     * @return imagepicker
     */
    public ImagePicker setImagePaths(ArrayList<String> imagePaths) {
        ConfigManager.getInstance().setImagePaths(imagePaths);
        return mImagePicker;
    }

    /**
     * Start ability
     *
     * @param requestCode
     * @param ability
     */
    public void start(Ability ability, int requestCode) {
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(TConstant.ENTRY_APP_BUNDLE_NAME)
                .withAbilityName(TConstant.IMAGE_PICKER_EXT)
                .build();
        Intent intent = new Intent();
        intent.setOperation(operation);
        ability.startAbilityForResult(intent, requestCode);
    }
}
