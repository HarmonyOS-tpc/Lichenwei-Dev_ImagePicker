/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.library.imagepicker.layouthelper;

import ohos.agp.components.element.ShapeElement;

public class GridLayoutHelper<T> extends BaseLayoutHelper<T> {
    private int mItemCount;
    private int mSpanCount;
    private ShapeElement element1;
    private int h;
    private ShapeElement element2;

    public GridLayoutHelper(int spanCount, int itemCount, ShapeElement background, int height) {
        this.mSpanCount = spanCount;
        setHeight(height);
        setItemCount(itemCount);
        setBackground(background);
    }

    int getSpanCount() {
        return mSpanCount;
    }

    @Override
    public int getItemCount() {
        return mItemCount;
    }

    @Override
    public void setBackground(ShapeElement background) {
        this.element1 = background;
    }

    @Override
    public ShapeElement getBackground() {
        return element1;
    }

    @Override
    public void setBackground1(ShapeElement background1) {
        this.element2 = background1;
    }

    @Override
    public ShapeElement getBackground1() {
        return element2;
    }

    int getTotalItemCount() {
        return mItemCount;
    }

    @Override
    public void setItemCount(int itemCount) {
        this.mItemCount = itemCount;
    }

    @Override
    public void setHeight(int height) {
        if (height == 0) {
            this.h = 100;
        } else {
            this.h = height;
        }
    }

    @Override

    public int getHeight() {
        return h;
    }
}

