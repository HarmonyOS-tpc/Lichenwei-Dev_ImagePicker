package com.lcw.library.imagepicker.view;

import com.lcw.library.imagepicker.ResourceTable;
import com.lcw.library.imagepicker.adapter.ImageFoldersItemProvider;
import com.lcw.library.imagepicker.data.MediaFolder;
import com.lcw.library.imagepicker.utils.Utils;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

import java.util.List;

/**
 * 图片文件夹列表窗口
 * Create by: chenWei.li
 * Date: 2018/8/25
 * Time: 上午1:30
 * Email: lichenwei.me@foxmail.com
 */
public class ImageFolderPopupWindow extends PopupDialog {
    private static final int DEFAULT_IMAGE_FOLDER_SELECT = 0;
    private Context mContext;
    private List<MediaFolder> mMediaFolderList;
    private ListContainer mFolderContainer;
    private ComponentContainer mDependentComponent;
    private ImageFoldersItemProvider mImageFoldersItemProvider;

    public ImageFolderPopupWindow(Context context, List<MediaFolder> mediaFolderList) {
        super(context, null);
        this.mContext = context;
        this.mMediaFolderList = mediaFolderList;
        initView();
    }

    private void initView() {
        mDependentComponent = (ComponentContainer) LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_window_image_folders, null, false);
        mFolderContainer = (ListContainer) mDependentComponent.findComponentById(ResourceTable.Id_lc_main_imageFolders);
        mImageFoldersItemProvider = new ImageFoldersItemProvider(mContext, mMediaFolderList, DEFAULT_IMAGE_FOLDER_SELECT);
        mFolderContainer.setItemProvider(mImageFoldersItemProvider);

        initPopupWindow(mDependentComponent);
    }

    private void initPopupWindow(Component view) {
        setCustomComponent(view);
        int[] screenSize = Utils.getScreenSize(mContext);
        int height = (int) (screenSize[1] * 0.6);
        setSize(screenSize[0], height);
    }

    public ImageFoldersItemProvider getFolderProvider() {
        return mImageFoldersItemProvider;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDependentComponent.getComponentParent().removeComponent(mDependentComponent);
    }
}
