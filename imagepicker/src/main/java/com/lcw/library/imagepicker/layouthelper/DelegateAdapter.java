/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.library.imagepicker.layouthelper;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class DelegateAdapter extends VirtualLayoutAdapter<VirtualLayoutManager.ImagePickerViewHolder> {
    private Context context;
    private VirtualLayoutManager mVirtualLayoutManager;
    private List<BaseLayoutHelper> mAdapters = new ArrayList<>();
    private int mItemCount;

    public DelegateAdapter(VirtualLayoutManager virtualLayoutManager) {
        mVirtualLayoutManager = virtualLayoutManager;
    }

    //Note: This will give total number of rows that are needed. Gria + linear all together.. etc..
    @Override
    public int getCount() {
        return mItemCount;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemComponentType(int position) {
        return mVirtualLayoutManager.getItemComponentType(position);
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        return mVirtualLayoutManager.getComponent(position, convertComponent, parent);
    }

    /**
     * Append adapters to the end
     *
     * @param adapters adapters will be appended
     */
    public void addAdapters(List<Adapter> adapters) {
        for (Adapter adapter : adapters) {
            mAdapters.add(adapter.onCreateLayoutHelper());
        }
        calculateItemCount();
        mVirtualLayoutManager.setVirtualLayoutManager(mAdapters);
    }

    private void calculateItemCount() {
        for (BaseLayoutHelper baseitem : mAdapters) {
            mItemCount += baseitem.getItemCount();
        }
    }

    public static abstract class Adapter extends BaseItemProvider {
        public abstract BaseLayoutHelper onCreateLayoutHelper();
    }
}