package com.lcw.library.imagepicker.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.app.Context;

/**
 * 正方形ImageView
 * Create by: chenWei.li
 * Date: 2018/9/1
 * Time: 下午10:12
 * Email: lichenwei.me@foxmail.com
 */
public class SquareImageView extends Image implements Component.DrawTask {

    public SquareImageView(Context context) {
        this(context, null);
        addDrawTask(this);
    }

    public SquareImageView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SquareImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
    }
}
