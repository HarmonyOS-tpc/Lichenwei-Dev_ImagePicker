package com.lcw.library.imagepicker.utils;

import ohos.agp.components.Image;
import ohos.utils.net.Uri;
import java.io.Serializable;

/**
 * 开放图片加载接口
 * Create by: chenWei.li
 * Date: 2018/8/30
 * Time: 下午11:07
 * Email: lichenwei.me@foxmail.com
 */
public interface ImageLoader extends Serializable {

    /**
     * Thumbnail loading scheme
     *
     * @param imageView
     * @param imagePath
     */
    void loadImage(Image imageView, String imagePath);

    /**
     * Large image loading scheme
     *
     * @param imageView
     * @param imagePath
     */
    void loadPreImage(Image imageView, String imagePath);


    /**
     * Image Uri
     *
     * @param imageView
     * @param uri
     */
    void loadImageUri(Image imageView, Uri uri);

    void loadImageResource(Image imageView, int imageId);

    /**
     * Cache Management
     */
    void clearMemoryCache();
}
