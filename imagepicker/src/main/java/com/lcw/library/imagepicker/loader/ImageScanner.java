package com.lcw.library.imagepicker.loader;

import com.lcw.library.imagepicker.data.MediaFile;
import com.lcw.library.imagepicker.manager.ConfigManager;
import ohos.aafwk.ability.DataUriUtils;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

/**
 * 媒体库扫描类(图片)
 * Create by: chenWei.li
 * Date: 2018/8/21
 * Time: 上午1:01
 * Email: lichenwei.me@foxmail.com
 */
public class ImageScanner extends AbsMediaScanner<MediaFile> {
    public ImageScanner(Context context) {
        super(context);
    }

    @Override
    protected Uri getScanUri() {
        return AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
    }

    @Override
    protected String[] getProjection() {
        return new String[]{
                AVStorage.Images.Media.DATA,
                AVStorage.Images.Media.MIME_TYPE,
                AVStorage.Images.Media.ID,
                AVStorage.Images.Media.DISPLAY_NAME,
                AVStorage.Images.Media.DATE_ADDED
        };
    }

    @Override
    protected String getSelection() {
        if (ConfigManager.getInstance().isFilterGif()) {
            //过滤GIF
            return AVStorage.Images.Media.MIME_TYPE + "=? or " + AVStorage.Images.Media.MIME_TYPE + "=?";
        }
        return AVStorage.Images.Media.MIME_TYPE + "=? or " + AVStorage.Images.Media.MIME_TYPE + "=?" + " or " + AVStorage.Images.Media.MIME_TYPE + "=?";
    }

    @Override
    protected String[] getSelectionArgs() {
        if (ConfigManager.getInstance().isFilterGif()) {
            return new String[]{"image/jpeg", "image/png"};
        }
        return new String[]{"image/jpeg", "image/png", "image/gif"};
    }

    @Override
    protected String getOrder() {
        return AVStorage.Images.Media.DATE_ADDED + " desc";
    }

    /**
     * 构建媒体对象
     *
     * @param cursor
     * @return media file
     */
    @Override
    protected MediaFile parse(ResultSet cursor) {
        String path = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.DATA));
        String mime = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.MIME_TYPE));
        int folderId = cursor.getInt(cursor.getColumnIndexForName(AVStorage.Images.Media.ID));
        String folderName = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.DISPLAY_NAME));
        long dateToken = cursor.getLong(cursor.getColumnIndexForName(AVStorage.Images.Media.DATE_ADDED));
        Uri externalUri = DataUriUtils.attachId(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, folderId);

        MediaFile mediaFile = new MediaFile();
        mediaFile.setPath(path);
        mediaFile.setMime(mime);
        mediaFile.setFolderId(folderId);
        mediaFile.setFolderName(folderName);
        mediaFile.setDateTaken(dateToken);
        mediaFile.setExternalUri(externalUri);

        return mediaFile;
    }
}