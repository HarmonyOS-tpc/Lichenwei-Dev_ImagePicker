/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lcw.library.imagepicker.utils;

public class TConstant {
    public final static int REQUEST_SELECT_IMAGES_CODE = 1002;
    public final static int RC_PICK_PICTURE_FROM_CAPTURE = 1003;
    public final static int REQUEST_PERMISSION_CAMERA_CODE = 1004;
    public static final int RESULT_CANCELED = 0;
    public static final int RESULT_OK = -1;

    public static final String ENTRY_APP_BUNDLE_NAME = "com.lcw.demo.imagepicker";
    public static final String IMAGE_PICKER_EXT = "ImagePickerAbilityExt";
    public static final String IMAGE_PRE_EXT = "ImagePreAbilityExt";
    public static final String CAMERA_ABILITY_EXT = "CameraAbilityExt";
    public static final String REQUEST_FILE_PATH = "camera_file";
    public static final String IMG_FILE_TYPE = ".jpg";
    public static final int GRID_SPAN_COUNT = 4;
    public static final int GRID_SPAN_HEIGHT = 250;
}