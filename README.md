## ImagePicker

## Introduction:
ImagePicker supports functions such as pictures, video single selection, multiple selection, multiple folder switching, large picture preview, and custom picture loader.

## Usage Instructions:

1. How to customize the image loader (indeterminate frame to make the frame more flexible, you need to implement the ImageLoader interface, if you need to display video, the Glide loading frame is recommended first, you can refer to the Demo implementation):
```
public class GlideLoader implements ImageLoader {
	//to do something
}
```
2. A line of code call:
```
ImagePicker.getInstance()
    .setTitle(ResUtil.getString(getAbility(), ResourceTable.String_title))//Set Title
    .showCamera(true)//Sets whether to display the photographing button.
    .showImage(true)//Set whether to display images.
    .showVideo(true)//Set whether to display videos.
    .filterGif(false)//Specifies whether to filter GIF images.
    .setMaxCount(9)//Sets the maximum number of images to be selected. The default 	value is 1.
    .setSingleType(true)//The image and video cannot be selected at the same time.
    .setImagePaths(mImagePaths)//Setting History Selection Records
    .setImageLoader(new GlideLoader())//Setting up a Custom Picture Loader
    .start(getAbility(), REQUEST_SELECT_IMAGES_CODE);//REQUEST_SELECT_IMAGES_CODE is the request code of Intent invoking.
```

3. How to get the selected picture collection:
```
@Override
protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
    if (requestCode == REQUEST_SELECT_IMAGES_CODE && resultCode == TConstant.RESULT_OK) {
		mImagePaths = resultData.getStringArrayListParam(ImagePicker.EXTRA_SELECT_IMAGES);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Path of the selected image：\n\n");
        for (String imagePath : mImagePaths) {
        	stringBuffer.append(imagePath).append("\n\n");
        }
        if (!TextUtils.isEmpty(stringBuffer.toString())) {
            mTextView.setText(stringBuffer.toString());
        }
    }
}
```
## Installation tutorial
```
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
        implementation 'io.openharmony.tpc.thirdlib:glide:1.0.4'

Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
    implementation 'io.openharmony.tpc.thirdlib:glide:1.0.4'
    implementation 'io.openharmony.tpc.thirdlib:Lichenwei-Dev_ImagePicker:1.0.0'
```